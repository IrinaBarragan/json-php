<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags 
    <meta name="description" content="">
    <meta name="author" content="">-->
    <?php ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

    <title>Cards</title>

    
    <!-- Bootstrap core CSS >
    <link href="css/bootstrap.min.css" rel="stylesheet"-->
    
   
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="custom.css" rel="stylesheet">
  </head>

  <body>


    <?php
    require 'functions.php';


$personnes = charger('file_csv.csv');
echo '<div class="flex">';

foreach( $personnes as $quelqun){

    echo '<div class="card" style="width: 18rem;">';
    echo '<img src='. $quelqun[5] .' class="card-img-top">';
    
    echo '<div class="card-body">';
    echo '<p class="card-text">'. $quelqun[0] . '</p>';
    
    echo '<p class="card-text">'. $quelqun[2] . '</p>';
    echo '<p class="card-text">'. $quelqun[3] . '</p>';
    echo '<p class="card-text">Born in ' . $quelqun[1] . '</p></div></div>';
    
   
}
'</div>'
?>

    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    

  </body>
</html>